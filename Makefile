ASM = nasm
ASMFLAGS = -felf64
LD = ld
RM = rm


main: main.o dict.o lib.o
	$(LD) -o $@ $^

main.o: main.asm colon.inc words.inc
	$(ASM) $(ASMFLAGS) $< -o $@

%.o: %.asm
	$(ASM) $(ASMFLAGS) $< -o $@
	        
clean:
	        $(RM) *.o main

.PHONY: clean
