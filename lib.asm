%define EXIT_CODE 60
%define SYSCALL_WRITE 1
%define STDOUT 1
%define STDERR 2
%define NULL_TERMINATOR 0


section .text
 
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error
global print_out
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_CODE
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, 0
    .loop:
    	cmp byte[rax+rdi], NULL_TERMINATOR
    	jz .get_string_len ; if zero
    	inc rax
    	jmp .loop
    .get_string_len:
    	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    
    mov rsi, rdi ; str addr
    mov rdx, rax ; len in bytes
    
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT
    syscall
    
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    
    mov rax, 1
    mov rdi, 1
    
    mov rdx, 1
    mov rsi, rsp ; top of stack(rdi) -> rsi
    syscall
    
    pop rdi
    ret
    

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA ; 0xA equals '\n'
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r9
    mov rax, rdi ; arg to rax
    mov r9, 10 ; divider to r9 
    push 0 ; pushing 0 to stack for check on null-terminate in the loop
    
    .loop:
    	mov rdx, 0 ; clear our remainder of the division register
    	div r9 ; div to 10, whole part -> rax, remainder(our number to convert) -> rdx
    	add rdx, '0' ; convert to decimal number
    	push rdx 
    	
    	test rax, rax 
    	jnz .loop ; if not ZF -> loop
    	
    .get_next_number:
    	pop rdi
    	test rdi, rdi
    	jnz .print_char ; if not 0 -> print char
    	pop r9 ; clear our deviator
    	ret ; if 0 -> ret
    
    .print_char:
    	call print_char ; print our char
    	jmp .get_next_number ; get our next number

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi ; comparing arg with 0 to get ZF, SF and other flags
    jge .print_unit ; if rdi >= 0 -> unsigned char -> can use already written print_uint 
    
    push rdi ; save our unsigned part to stack
    mov rdi, '-' ; '-' char -> to rdi (0x2D = '-' in ASCII)
    call print_char ; print '-' char
    pop rdi ; get back our unsigned char
    neg rdi ; get twos-complement 
    
    .print_unit:
        jmp print_uint ; print our unsigned char

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax ; clearing rax because default return is 0
    
    .loop:
    	mov r8b, byte[rdi] ; first str -> r8b
    	cmp r8b, byte[rsi] ; compare first and second str
    	jne .end ; if not equals -> end
    	 
    	test r8b, r8b ; compare first str and 0(NUL to end)
    	je .equals
    	
    	inc rdi
    	inc rsi
    	jmp .loop ; check all chars in strings
    
    .equals:
    	mov rax, SYSCALL_WRITE ; strings are equals and have same len
    
    .end:
    	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax ; set number of sys read(0)
    xor rdi, rdi ; set number of stdout
    
    push 0 ; reserve place where we will read
    mov rdx, 1 ; how many bytes we need to read
    
    mov rsi, rsp ; addr of reserved stack place -> rsi
    syscall ; read char
    
    pop rax ; head of stack -> rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx ; symbols counter
    
    .loop:
    	push rdi
    	push rsi
    	push rcx
    	call read_char ; read char from stdin
    	pop rcx
    	pop rsi
    	pop rdi
    	
    	cmp rax, 0x20 ; if space(' ')
    	je .check_on_except_symbols
    	cmp rax, 0x9 ; if tab
    	je .check_on_except_symbols
    	cmp rax, 0xA ; if '\n'
    	je .check_on_except_symbols
    	
    	cmp rax, 0x0 ; if NUL
    	je .end
    	
    	mov [rdi+rcx], rax ; write our buffer
    	inc rcx ; inc symbols counter
    	cmp rcx, rsi ; check on buffer overflow
    	jl .loop ; if not overflowed
    	mov rax, 0 ; if overflowed 0 -> rax and ret
    	ret 
    	
    .check_on_except_symbols:
    	cmp rcx, 0
    	jz .loop
    
    .end:
    	mov byte[rdi+rcx], 0 ; write null-character to word
    	mov rdx, rcx ; word len
    	mov rax, rdi ; buffer start addr
    	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
    xor r8, r8

    .loop:
    	mov r8b, byte[rdi + rcx]
    	cmp r8b, '0'
        jl .end
        cmp r8b, '9'
        jg .end
    
        sub r8b, '0'
    
        inc rcx
        imul rax, 10
        add rax, r8
        jmp .loop
    .end:
        mov rdx, rcx
        ret
    	



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-' ; check on first char -> if '-' first -> int is negative
    jne parse_uint ; parse if positive
    inc rdi
    
    call parse_uint ; if negative
    inc rdx ; inc char len because 
    neg rax  
    
    ret
    
    

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx ; counter
    
    push rdi ; save caller-save regs
    push rsi
    push rdx
    
    call string_length
    
    pop rdx ; get back regs
    pop rsi
    pop rdi
    
    cmp rax, rdx ; comparing len of string(in rax after string_length call) and size of buffer
    jge .buffer_overflow ; if len of string greater than buffer -> buffer_overflow
    
    
    .loop:
    	xor r8, r8
    	mov r8b, byte[rdi+rcx]
    	mov byte[rsi+rcx], r8b ; char to buffer
    	
    	cmp r8b, 0 ; if null -> end
    	je .end
    	
    	inc rcx
    	jmp .loop
    
    .buffer_overflow:
    	xor rax, rax
    	jmp .end
    
    .end:
    	ret
    	
    	
print_error:
    xor rax, rax
	
    push rdi
    call string_length
    pop rsi
	
    mov rdx, rax
    mov rdi, STDERR
    mov rax, SYSCALL_WRITE
    
    syscall
    
    ret
