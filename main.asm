%include "lib.inc"
%include "words.inc"

%define BUFFER_SIZE 256

global _start

section .data

section .rodata
	start_msg: db 'Input the key: ', 10, 0
	error_key_message: db 'Input error. Length of key should be less than 256', 10, 0
	not_found_message: db 'Key not found', 10, 0


section .text
extern find_word

_start:
	mov rdi, start_msg
	call print_string
	
	mov rdi, rsp 
	mov rsi, BUFFER_SIZE 
	call read_word
	test rax, rax ; check len of key 
	je .bad_key
	mov rsi, next
	mov rdi, rax
	call find_word
	
	add rsp, BUFFER_SIZE ; restore our stack
	cmp rax, 0 ; trying to find word
	je .not_found	
	add rax, 8  
	push rax 
	mov rdi, [rsp] ; Key pointer -> rdi 
	call string_length
	pop rdi
	add rdi, rax 
	inc rdi 
	call print_string
	call print_newline
	jmp _start
	

        .bad_key:
    	    mov rdi, error_key_message
	    call print_error
	    call exit
	
    	.not_found:
    	    mov rdi, not_found_message
	    call print_error
	    call exit
