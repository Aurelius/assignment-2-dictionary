%include "colon.inc"

section .data


colon "checkers", checkers
db "В детстве ты думаешь, что жизнь - это шашки: все равны, и только те, кто стремится вперёд, попадают в дамки.", 0

colon "chess", chess
db "Повзрослев, ты думаешь, что жизнь как шахматы: кто-то с самого начала может больше других, а ты можешь проиграть, даже если у тебя ещё остались фигуры.", 0

colon "backgammon", backgammon
db "И лишь в старости ты понимаешь, что жизнь - это нарды: лучше всех играют армяне.", 0 
